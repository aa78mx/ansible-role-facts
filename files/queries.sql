-- List mounts as table
WITH a AS (
SELECT data->'ansible_facts'->'ansible_mounts' AS mounts FROM facts_json)
SELECT elem->>'device' AS device, elem->>'mount' AS mount, (elem->>'size_total')::numeric AS size_total, elem->>'size_available' AS size_available FROM a,
jsonb_array_elements(mounts) elem;

-- List distinct 'Images' objects as jsonb
SELECT DISTINCT
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'disk_usage'->'Images' AS docker_images
FROM facts_json;

-- List distinct 'RepoTags' as text
SELECT DISTINCT elem->>'RepoTags' as repotags
FROM facts_json,
jsonb_array_elements(data->'ansible_facts'->'ansible_local'->'docker_host_info'->'disk_usage'->'Images') elem

-- Split 'replications' to key/value as record
SELECT jsonb_each(data->'ansible_facts'->'ansible_local'->'postgresql_info'->'replications')
FROM facts_json ORDER BY id DESC;

-- Split 'replications' to key/value
SELECT key as text, value as jsonb
FROM facts_json, jsonb_each(data->'ansible_facts'->'ansible_local'->'postgresql_info'->'replications')
ORDER BY id DESC;

-- List dublicates per server per day			   
SELECT time, query_time, nodename,  ROW_NUMBER() OVER (PARTITION BY TIME, nodename ORDER BY query_time DESC) AS rn
FROM facts ORDER BY query_time DESC;

-- Only keep most recent record per day per nodename
WITH ranked AS (
SELECT id, ROW_NUMBER() OVER (PARTITION BY TIME, nodename ORDER BY query_time DESC) AS rn
FROM facts)
DELETE FROM facts
WHERE id IN
(SELECT id FROM ranked WHERE rn >= 2);

-- List lldp info (interfaces) Grafana syntax
set session my.var1 = '$Search';

WITH A AS (
SELECT jsonb_build_object('interface',key) AS interface, value, tsv, nodename, service, environment, system_vendor, time 
	FROM facts, jsonb_each(lldp)
), B AS (
SELECT interface->>'interface' AS interface, value, tsv, nodename, service, environment, system_vendor, time FROM A)
SELECT time, nodename,interface, value->'chassis'->>'name' AS remote_switch, 
value->'port'->>'ifname' AS remote_interface,
value->'port'->>'local' AS remote_interface
FROM B
WHERE $__timeFilter(time)
AND (tsv @@ websearch_to_tsquery('simple', current_setting('my.var1')) OR NULLIF(current_setting('my.var1'), '') IS null)
AND (service IN ($Service) OR NULLIF(TRIM(CONCAT($Service)), '') IS null)
AND (environment IN ($Environment) OR NULLIF(TRIM(CONCAT($Environment)), '') IS null)
AND (system_vendor IN ($Vendor) OR NULLIF(TRIM(CONCAT($Vendor)), '') IS null)
--GROUP BY 1,2,3
ORDER BY 1;

-- List lldp info (interfaces) Grafana generated query
set session my.var1 = '';

WITH A AS (
SELECT jsonb_build_object('interface',key) AS interface, value, tsv, nodename, service, environment, system_vendor, time 
	FROM facts, jsonb_each(lldp)
), B AS (
SELECT interface->>'interface' AS interface, value, tsv, nodename, service, environment, system_vendor, time FROM A)
SELECT time, nodename,interface, value->'chassis'->>'name' AS remote_switch, 
value->'port'->>'ifname' AS remote_interface,
value->'port'->>'local' AS remote_interface
FROM B
WHERE time BETWEEN '2020-03-01T10:00:00.000Z' AND '2020-09-01T10:00:00.000Z'
AND (tsv @@ websearch_to_tsquery('simple', current_setting('my.var1')) OR NULLIF(current_setting('my.var1'), '') IS null)
AND (service IN ('waarneming') OR NULLIF(TRIM(CONCAT('waarneming')), '') IS null)
AND (environment IN (null) OR NULLIF(TRIM(CONCAT(null)), '') IS null)
AND (system_vendor IN (null) OR NULLIF(TRIM(CONCAT(null)), '') IS null)
ORDER BY 1;

-- Update metadata object
UPDATE facts_json SET "data" = jsonb_set(data, '{ansible_facts,ansible_local,custom_facts,metadata,environment}', '"test"') 
--WHERE data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'environment' = 'tst';
WHERE data->'ansible_facts'->>'ansible_nodename' = 'waarneming-db-5';

-- Update facts table
UPDATE facts SET environment = 'test' WHERE nodename = 'waarneming-db-5';

-- Delete record from facts_json
DELETE FROM facts_json WHERE data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'service' = 'linnaeus'; 

-- Delete record from facts table
DELETE FROM facts WHERE service = 'linnaeus'; 

-- Delete based on date
DELETE FROM facts_json WHERE
date_trunc('day', (data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz) BETWEEN '2020-10-20' and '2020-10-20'

-- Delete based on date
DELETE FROM facts WHERE time BETWEEN '2020-10-20' and '2020-10-20';

-- Create copy of table to test
CREATE facts_json_copy AS SELECT * FROM facts_json;

-- Add key 'environment' with value from 'status'
UPDATE facts_json
SET data = jsonb_set(data, '{ansible_facts,ansible_local,custom_facts,metadata}', data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata' || jsonb_build_object('environment', data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->'status'))

-- Add key 'service' with value from 'project'
UPDATE facts_json 
SET data = jsonb_set(data, '{ansible_facts,ansible_local,custom_facts,metadata}', data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata' || jsonb_build_object('service', data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->'project'))

-- Remove 'project' object from metadata
UPDATE facts_json 
SET data = jsonb_set(data, '{ansible_facts,ansible_local,custom_facts,metadata}', data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata' #- '{project}')

-- Build json object
SELECT jsonb_build_object('service', data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->'project') FROM facts_json;

-- List 'databases' as jsonb
WITH A AS(
SELECT jsonb_build_object('database',key) AS db, value
FROM facts_json, jsonb_each(data->'ansible_facts'->'ansible_local'->'postgresql_info'->'databases'))
SELECT db || value FROM A;

-- Build json object will be added to top level
UPDATE facts_json SET data = data - 'project' || jsonb_build_object('service', data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->'project')

-- Add to metadata a key with name 'service', use value from key 'project'
SELECT data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata' || jsonb_build_object('service', data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->'project') FROM facts_json;

-- Add {"foo":"bar"} to metadata
UPDATE facts_json
SET data = jsonb_set(data, '{ansible_facts,ansible_local,custom_facts,metadata}', data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata' || '{"foo":"bar"}')


