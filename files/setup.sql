-- Drop table facts
DROP TABLE facts;

-- Create facts table from data in facts_json table
CREATE TABLE facts AS
SELECT
id AS id,
(data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz AS query_time,
date_trunc('day', (data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz) AS time,
data->'ansible_facts'->>'ansible_nodename' AS nodename,
data->'ansible_facts'->>'ansible_product_uuid' AS product_uuid,
data->'ansible_facts'->>'ansible_all_ipv4_addresses' AS all_ipv4_addresses,
data->'ansible_facts'->>'ansible_bios_version' AS bios_version,
data->'ansible_facts'->'ansible_lsb'->>'description' AS lsb,
data->'ansible_facts'->>'ansible_kernel' AS kernel,
data->'ansible_facts'->>'ansible_distribution' AS distribution,
data->'ansible_facts'->>'ansible_distribution_release' AS distribution_release,
data->'ansible_facts'->>'ansible_system_vendor' AS system_vendor,
data->'ansible_facts'->>'ansible_product_name' AS product_name,
data->'ansible_facts'->>'ansible_form_factor' AS form_factor,
data->'ansible_facts'->>'ansible_product_version' AS product_version,
data->'ansible_facts'->>'ansible_product_serial' AS product_serial,
(data->'ansible_facts'->>'ansible_memtotal_mb')::int/1024 AS memtotal_gb,
(data->'ansible_facts'->>'ansible_processor_count')::int AS processor_count,
(data->'ansible_facts'->>'ansible_processor_cores')::int AS processor_cores,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'service' AS service,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'owner' AS owner,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'environment' AS environment,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'asset_tag' AS asset_tag,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'management_address' AS management_address,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'description' AS description,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'topdesk' AS topdesk,
string_to_array((data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'tags'), ', ') AS tags,
(data->'ansible_facts'->'ansible_local'->'custom_facts'->'apt'->>'apt_security_updates')::int AS apt_security_updates,
(data->'ansible_facts'->'ansible_local'->'custom_facts'->'apt'->>'reboot_required')::int AS reboot_required,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'misc'->>'ansible_ssh_host' AS ansible_ssh_host,
(data->'ansible_facts'->'ansible_uptime_seconds')::int/86400 AS uptime_days,
data->'ansible_facts'->>'ansible_virtualization_type' AS virtualization_type,
data->'ansible_facts'->'ansible_local'->'restic'->>'snapshot_id' AS restic_snapshot_id,
(data->'ansible_facts'->'ansible_local'->'restic'->>'timestamp')::timestamptz AS restic_end_backup_timestamp,
(data->'ansible_facts'->'ansible_local'->'restic'->>'total_duration')::numeric AS restic_total_duration,
(data->'ansible_facts'->'ansible_local'->'restic'->>'total_bytes_processed')::numeric AS restic_total_bytes_processed,
(data->'ansible_facts'->'ansible_local'->'restic'->>'total_files_processed')::numeric AS restic_total_files_processed,
data->'ansible_facts'->'facter_ec2_metadata'->>'public-ipv4' AS public_ipv4,
data->'ansible_facts'->'facter_ec2_metadata'->>'instance-id' AS instance_id,
data->'ansible_facts'->'facter_ec2_metadata'->>'instance-type' AS instance_type,
data->'ansible_facts'->'facter_ec2_metadata'->>'security-groups' AS security_groups,
data->'ansible_facts'->'ansible_local'->'postgresql_info' AS postgresql_info,
data->'ansible_facts'->'ansible_local'->'scripts_info' AS scripts_info,
data->'ansible_facts'->'ansible_local'->'package'->'ansible_facts' AS packages,
--data->'ansible_facts'->'ansible_local'->'postgresql_info'->'settings'->'server_version'->>'setting' AS postgresql_version,
--data->'ansible_facts'->'ansible_local'->'postgresql_info'->'databases' AS postgresql_databases,
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'host_info'->>'ServerVersion' AS docker_version,
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'disk_usage'->'Images' AS docker_images,
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'disk_usage'->'Containers' AS docker_containers,
data->'ansible_facts'->'ansible_local'->'lldp' AS lldp,
data->'ansible_facts'->'ansible_local'->'mysql_info' AS mysql_info,
(data->'ansible_facts'->'facter_load_averages'->>'15m')::numeric AS facter_load_average_15m,
data->'ansible_facts'->'ansible_mounts' AS mounts,
substr(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'time',1,19)::timestamptz AS restic_stats_time,
(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'snapshot_count')::numeric AS restic_stats_snapshot_count,
(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'total_size')::numeric AS restic_stats_total_size,
(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'total_file_count')::numeric AS restic_stats_total_file_count,
data->'ansible_facts'->'ansible_local'->'restic_stats'->>'short_id' AS restic_stats_short_id,
data->'ansible_facts'->'ansible_local'->'services' AS services
FROM facts_json
WHERE data->'ansible_facts'->>'ansible_nodename' is not null
WITH NO DATA;

-- Add constraint on id
ALTER TABLE facts ADD CONSTRAINT facts_constraint_id UNIQUE ("id");

-- Add column tsv
ALTER TABLE facts ADD COLUMN tsv tsvector;

-- Create function create tsvectors
CREATE OR REPLACE FUNCTION create_tsv_facts_funct() RETURNS trigger AS $$
begin
  new.tsv :=
  coalesce(to_tsvector('pg_catalog.simple', new.docker_images),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.nodename,'')),'') ||
  coalesce(to_tsvector('pg_catalog.simple', coalesce(new.all_ipv4_addresses,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.bios_version,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.lsb,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.kernel,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.distribution,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.distribution_release,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.system_vendor,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.product_name,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.form_factor,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.product_version,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.product_serial,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.service,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.owner,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.environment,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.asset_tag,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.management_address,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.description,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.topdesk,'')),'') ||
 	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.ansible_ssh_host,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.virtualization_type,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.restic_snapshot_id,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.public_ipv4,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.instance_id,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.instance_type,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.security_groups,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.docker_version,'')),'') ||
	coalesce(to_tsvector('pg_catalog.simple', coalesce(new.restic_stats_short_id,'')),'');
  RETURN new;
END
$$ LANGUAGE plpgsql;

-- Create trigger create tsvectors
CREATE TRIGGER create_tsv_facts_trigger BEFORE INSERT OR UPDATE
ON facts FOR EACH ROW EXECUTE PROCEDURE create_tsv_facts_funct();










-- Only keep most recent records when they are inserted into facts_json table
CREATE OR REPLACE FUNCTION a_keep_most_recent_per_day_table_facts_json_func() 
  RETURNS TRIGGER
AS
$$
BEGIN
WITH json_to_table AS (SELECT id,
date_trunc('day', (data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz) AS time,
data->'ansible_facts'->>'ansible_nodename' AS nodename,
(data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz AS query_time
FROM facts_json),
ranked AS (
    SELECT id, row_number() OVER (PARTITION BY time, nodename ORDER BY query_time DESC) AS rn
    FROM json_to_table
    --where id <> new.id
    --and nodename = new.data->'ansible_facts'->>'ansible_nodename'
  )
  DELETE FROM facts_json
  WHERE id IN (SELECT id 
               FROM ranked 
               WHERE rn >= 2);
  RETURN new;
END;
$$
LANGUAGE plpgsql;

-- Drop trigger
DROP TRIGGER IF EXISTS a_keep_most_recent_per_day_table_facts_json_trigger on facts_json; 

-- Create trigger, only keep most recent records when they are inserted into facts_json table
CREATE TRIGGER a_keep_most_recent_per_day_table_facts_json_trigger 
  AFTER INSERT ON facts_json
  FOR EACH STATEMENT EXECUTE PROCEDURE a_keep_most_recent_per_day_table_facts_json_func();











-- Create function to insert data into facts table on insert into facts_json table
CREATE OR REPLACE FUNCTION b_insert_into_facts_func()
  RETURNS trigger AS
$BODY$
BEGIN
  INSERT INTO facts
  SELECT
id AS id,
(data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz AS query_time,
date_trunc('day', (data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz) AS time,
data->'ansible_facts'->>'ansible_nodename' AS nodename,
data->'ansible_facts'->>'ansible_product_uuid' AS product_uuid,
data->'ansible_facts'->>'ansible_all_ipv4_addresses' AS all_ipv4_addresses,
data->'ansible_facts'->>'ansible_bios_version' AS bios_version,
data->'ansible_facts'->'ansible_lsb'->>'description' AS lsb,
data->'ansible_facts'->>'ansible_kernel' AS kernel,
data->'ansible_facts'->>'ansible_distribution' AS distribution,
data->'ansible_facts'->>'ansible_distribution_release' AS distribution_release,
data->'ansible_facts'->>'ansible_system_vendor' AS system_vendor,
data->'ansible_facts'->>'ansible_product_name' AS product_name,
data->'ansible_facts'->>'ansible_form_factor' AS form_factor,
data->'ansible_facts'->>'ansible_product_version' AS product_version,
data->'ansible_facts'->>'ansible_product_serial' AS product_serial,
(data->'ansible_facts'->>'ansible_memtotal_mb')::int/1024 AS memtotal_gb,
(data->'ansible_facts'->>'ansible_processor_count')::int AS processor_count,
(data->'ansible_facts'->>'ansible_processor_cores')::int AS processor_cores,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'service' AS service,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'owner' AS owner,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'environment' AS environment,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'asset_tag' AS asset_tag,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'management_address' AS management_address,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'description' AS description,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'topdesk' AS topdesk,
string_to_array((data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'tags'), ', ') AS tags,
(data->'ansible_facts'->'ansible_local'->'custom_facts'->'apt'->>'apt_security_updates')::int AS apt_security_updates,
(data->'ansible_facts'->'ansible_local'->'custom_facts'->'apt'->>'reboot_required')::int AS reboot_required,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'misc'->>'ansible_ssh_host' AS ansible_ssh_host,
(data->'ansible_facts'->'ansible_uptime_seconds')::int/86400 AS uptime_days,
data->'ansible_facts'->>'ansible_virtualization_type' AS virtualization_type,
data->'ansible_facts'->'ansible_local'->'restic'->>'snapshot_id' AS restic_snapshot_id,
(data->'ansible_facts'->'ansible_local'->'restic'->>'timestamp')::timestamptz AS restic_end_backup_timestamp,
(data->'ansible_facts'->'ansible_local'->'restic'->>'total_duration')::numeric AS restic_total_duration,
(data->'ansible_facts'->'ansible_local'->'restic'->>'total_bytes_processed')::numeric AS restic_total_bytes_processed,
(data->'ansible_facts'->'ansible_local'->'restic'->>'total_files_processed')::numeric AS restic_total_files_processed,
data->'ansible_facts'->'facter_ec2_metadata'->>'public-ipv4' AS public_ipv4,
data->'ansible_facts'->'facter_ec2_metadata'->>'instance-id' AS instance_id,
data->'ansible_facts'->'facter_ec2_metadata'->>'instance-type' AS instance_type,
data->'ansible_facts'->'facter_ec2_metadata'->>'security-groups' AS security_groups,
data->'ansible_facts'->'ansible_local'->'postgresql_info' AS postgresql_info,
data->'ansible_facts'->'ansible_local'->'scripts_info' AS scripts_info,
data->'ansible_facts'->'ansible_local'->'package'->'ansible_facts' AS packages,
--data->'ansible_facts'->'ansible_local'->'postgresql_info'->'settings'->'server_version'->>'setting' AS postgresql_version,
--data->'ansible_facts'->'ansible_local'->'postgresql_info'->'databases' AS postgresql_databases,
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'host_info'->>'ServerVersion' AS docker_version,
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'disk_usage'->'Images' AS docker_images,
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'disk_usage'->'Containers' AS docker_containers,
data->'ansible_facts'->'ansible_local'->'lldp' AS lldp,
data->'ansible_facts'->'ansible_local'->'mysql_info' AS mysql_info,
(data->'ansible_facts'->'facter_load_averages'->>'15m')::numeric AS facter_load_average_15m,
data->'ansible_facts'->'ansible_mounts' AS mounts,
substr(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'time',1,19)::timestamptz AS restic_stats_time,
(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'snapshot_count')::numeric AS restic_stats_snapshot_count,
(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'total_size')::numeric AS restic_stats_total_size,
(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'total_file_count')::numeric AS restic_stats_total_file_count,
data->'ansible_facts'->'ansible_local'->'restic_stats'->>'short_id' AS restic_stats_short_id,
data->'ansible_facts'->'ansible_local'->'services' AS services
FROM facts_json
WHERE data->'ansible_facts'->>'ansible_nodename' IS NOT NULL AND
date_trunc('day', (data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz) = current_date::timestamptz
ON CONFLICT ("id")
DO NOTHING;
RETURN NULL;
END;
$BODY$
LANGUAGE plpgsql;

-- Drop trigger on facts_json table
DROP TRIGGER IF EXISTS b_insert_into_facts_trigger on facts_json;

-- Create trigger on facts_json table
CREATE TRIGGER b_insert_into_facts_trigger
AFTER INSERT
ON facts_json
FOR EACH STATEMENT EXECUTE PROCEDURE b_insert_into_facts_func();

-- Create trigger, only keep most recent records when they are inserted into facts table
CREATE OR REPLACE FUNCTION keep_most_recent_per_day_table_facts_func() 
  RETURNS TRIGGER
AS
$$
BEGIN
  WITH ranked AS (
    SELECT id, row_number() OVER (PARTITION BY time, nodename ORDER BY query_time DESC) AS rn
    FROM facts
    --where id <> new.id
    --and nodename = new.nodename 
  )
  DELETE FROM facts
  WHERE id IN (SELECT id 
               FROM ranked 
               WHERE rn >= 2);
  RETURN new;
END;
$$
LANGUAGE plpgsql;

-- Create trigger, only keep most recent records when they are inserted into facts table
CREATE TRIGGER keep_most_recent_per_day_table_facts_trigger 
AFTER INSERT ON facts
FOR EACH STATEMENT EXECUTE PROCEDURE keep_most_recent_per_day_table_facts_func();

--####################################################################################################  
  
-- Manually insert data
INSERT INTO facts
SELECT
id AS id,
(data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz AS query_time,
date_trunc('day', (data->'ansible_facts'->'ansible_date_time'->>'iso8601')::timestamptz) AS time,
data->'ansible_facts'->>'ansible_nodename' AS nodename,
data->'ansible_facts'->>'ansible_product_uuid' AS product_uuid,
data->'ansible_facts'->>'ansible_all_ipv4_addresses' AS all_ipv4_addresses,
data->'ansible_facts'->>'ansible_bios_version' AS bios_version,
data->'ansible_facts'->'ansible_lsb'->>'description' AS lsb,
data->'ansible_facts'->>'ansible_kernel' AS kernel,
data->'ansible_facts'->>'ansible_distribution' AS distribution,
data->'ansible_facts'->>'ansible_distribution_release' AS distribution_release,
data->'ansible_facts'->>'ansible_system_vendor' AS system_vendor,
data->'ansible_facts'->>'ansible_product_name' AS product_name,
data->'ansible_facts'->>'ansible_form_factor' AS form_factor,
data->'ansible_facts'->>'ansible_product_version' AS product_version,
data->'ansible_facts'->>'ansible_product_serial' AS product_serial,
(data->'ansible_facts'->>'ansible_memtotal_mb')::int/1024 AS memtotal_gb,
(data->'ansible_facts'->>'ansible_processor_count')::int AS processor_count,
(data->'ansible_facts'->>'ansible_processor_cores')::int AS processor_cores,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'service' AS service,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'owner' AS owner,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'environment' AS environment,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'asset_tag' AS asset_tag,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'management_address' AS management_address,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'description' AS description,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'topdesk' AS topdesk,
string_to_array((data->'ansible_facts'->'ansible_local'->'custom_facts'->'metadata'->>'tags'), ', ') AS tags,
(data->'ansible_facts'->'ansible_local'->'custom_facts'->'apt'->>'apt_security_updates')::int AS apt_security_updates,
(data->'ansible_facts'->'ansible_local'->'custom_facts'->'apt'->>'reboot_required')::int AS reboot_required,
data->'ansible_facts'->'ansible_local'->'custom_facts'->'misc'->>'ansible_ssh_host' AS ansible_ssh_host,
(data->'ansible_facts'->'ansible_uptime_seconds')::int/86400 AS uptime_days,
data->'ansible_facts'->>'ansible_virtualization_type' AS virtualization_type,
data->'ansible_facts'->'ansible_local'->'restic'->>'snapshot_id' AS restic_snapshot_id,
(data->'ansible_facts'->'ansible_local'->'restic'->>'timestamp')::timestamptz AS restic_end_backup_timestamp,
(data->'ansible_facts'->'ansible_local'->'restic'->>'total_duration')::numeric AS restic_total_duration,
(data->'ansible_facts'->'ansible_local'->'restic'->>'total_bytes_processed')::numeric AS restic_total_bytes_processed,
(data->'ansible_facts'->'ansible_local'->'restic'->>'total_files_processed')::numeric AS restic_total_files_processed,
data->'ansible_facts'->'facter_ec2_metadata'->>'public-ipv4' AS public_ipv4,
data->'ansible_facts'->'facter_ec2_metadata'->>'instance-id' AS instance_id,
data->'ansible_facts'->'facter_ec2_metadata'->>'instance-type' AS instance_type,
data->'ansible_facts'->'facter_ec2_metadata'->>'security-groups' AS security_groups,
data->'ansible_facts'->'ansible_local'->'postgresql_info' AS postgresql_info,
data->'ansible_facts'->'ansible_local'->'package'->'ansible_facts' AS packages,
--data->'ansible_facts'->'ansible_local'->'postgresql_info'->'settings'->'server_version'->>'setting' AS postgresql_version,
--data->'ansible_facts'->'ansible_local'->'postgresql_info'->'databases' AS postgresql_databases,
data->'ansible_facts'->'ansible_local'->'scripts_info' AS scripts_info,
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'host_info'->>'ServerVersion' AS docker_version,
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'disk_usage'->'Images' AS docker_images,
data->'ansible_facts'->'ansible_local'->'docker_host_info'->'disk_usage'->'Containers' AS docker_containers,
data->'ansible_facts'->'ansible_local'->'lldp' AS lldp,
data->'ansible_facts'->'ansible_local'->'mysql_info' AS mysql_info,
(data->'ansible_facts'->'facter_load_averages'->>'15m')::numeric AS facter_load_average_15m,
data->'ansible_facts'->'ansible_mounts' AS mounts,
substr(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'time',1,19)::timestamptz AS restic_stats_time,
(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'snapshot_count')::numeric AS restic_stats_snapshot_count,
(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'total_size')::numeric AS restic_stats_total_size,
(data->'ansible_facts'->'ansible_local'->'restic_stats'->>'total_file_count')::numeric AS restic_stats_total_file_count,
data->'ansible_facts'->'ansible_local'->'restic_stats'->>'short_id' AS restic_stats_short_id,
data->'ansible_facts'->'ansible_local'->'services' AS services
FROM facts_json
WHERE data->'ansible_facts'->>'ansible_nodename' is not null
ON CONFLICT ("id")
DO NOTHING;

-- Indexes...
CREATE INDEX idx_facts_id ON facts(id);
CREATE INDEX idx_facts_json_id ON facts_json(id);
CREATE INDEX ON facts_json((data->'ansible_facts'->>'ansible_nodename'));
