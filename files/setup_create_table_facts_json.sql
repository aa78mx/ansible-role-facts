-- Create table for raw json data
CREATE TABLE IF NOT EXISTS facts_json (
  id INT GENERATED ALWAYS AS IDENTITY,
  data jsonb
);

-- Import json data 
COPY facts_json (data) FROM '/tmp/ansible_facts_assembled.json' CSV QUOTE e'\x01' DELIMITER e'\x02';  
