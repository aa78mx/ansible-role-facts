ansible-role-facts
=============

This role creates custom facts and gathers application facts using Ansible. These facts are pushed into a PostgreSQL database. Use the data to display facts about your hosts in for example Grafana.

```mermaid
graph LR;
    ansible-facts --> json --> PostgreSQL --> Grafana;
```


Requirements
------------

Facts from the following applications will be gathered:
- lldp (package lldpd required)
- apt updates
- postgres
- docker
- restic

Role Variables
--------------
```yaml
custom_facts:
  - section: metadata
    settings:
      - key: environment
        value: production
      - key: service
        value: analytics
```

Example Playbook
----------------


```yaml
---
- name: Gather and create facts
  hosts: all
  become: true
  tasks:
    - include_role:
        name: facts
        apply:
          tags: facts
      tags: always
...
```

Create ssh config file
----------------------
```
ansible-playbook playbooks/facts.yml -t ssh_config_file -l analytics-facts-1
```

Gather package facts
--------------------
Package facts are not gathered every run because of the amount of data.
To gather package facts:
```
ansible-playbook playbooks/facts.yml -t package_facts,gather,write,insert
```


## License

Apache License 2.0

## Author Information

* Rudi Broekhuizen


